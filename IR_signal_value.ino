#include <IRremote.h>

const int RECV_PIN = 8;
IRrecv irrecv(RECV_PIN);
decode_results results;

void setup(){
  Serial.begin(9600);
  irrecv.enableIRIn(); //Begin the receiving process
  irrecv.blink13(true); // Blinking led during reception.
}

void loop(){
  if (irrecv.decode(&results)){
        Serial.println(results.value, HEX);
        irrecv.resume();
  }
}
